from django.forms import ModelForm
from recipes.models import Recipe, Edit


class RecipeForm(ModelForm):
    class Meta:
        model = Recipe
        fields = [
        "title",
        "picture",
        "description",
        ]

class RecipeEdit(ModelForm):
    class Meta:
        model = Edit
        fields = [
        "title",
        "picture",
        "description",
        ]
